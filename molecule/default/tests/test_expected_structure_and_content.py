import pytest
import re


@pytest.mark.parametrize("filename", [
    ("/home/ansible/.qmail-john"),
    ("/home/ansible/.qmail-jane"),
    ("/home/ansible/.qmail-bob"),
    ("/home/ansible/.qmail-alice"),
])
def test_qmail_files_present(host, filename):
    assert_mail_file_present(host.file(filename), 0o644)


def assert_mail_file_present(file, mode):
    assert file.exists
    assert 'ansible' == file.user
    assert 'ansible' == file.group
    assert mode == file.mode


@pytest.mark.parametrize("user", [
    'john',
    'jane',
    'bob',
    'alice',
])
def test_filter_files_present(host, user):
    assert_mail_file_present(
        host.file(get_filter_filename(user)),
        0o600
    )


def get_filter_filename(user):
    return '/home/ansible/.{}-filter'.format(user)


@pytest.mark.parametrize("user,domain", [
    ('john', None),
    ('bob', None),
    ('jane', 'family-doe.com'),
    ('alice', 'wonder.land'),
])
def test_filter_files_configure_domain(host, user, domain):
    if domain is None:
        expected_pattern = '^FROM="{}@ansible.uber.space"'.format(user)
    else:
        expected_pattern = '^FROM="{}@{}"'.format(user, domain)
    assert_regexp_match_in_file(
        host.file(get_filter_filename(user)),
        expected_pattern
    )


def assert_regexp_match_in_file(file, expected_pattern):
    actual_content = file.content_string
    assert \
        re.search(expected_pattern, actual_content, re.MULTILINE) is not None


@pytest.mark.parametrize("user,is_log_enabled", [
    ('john', False),
    ('jane', False),
    ('bob', True),
    ('alice', True),
])
def test_filter_files_configure_log(host, user, is_log_enabled):
    if is_log_enabled:
        expected_regexp = '^logfile "\\$HOME/{}-filter.log"'.format(user)
    else:
        expected_regexp = '^# logfile "\\$HOME/{}-filter.log"'.format(user)
    assert_regexp_match_in_file(
        host.file(get_filter_filename(user)),
        expected_regexp
    )
