# macrominds/provision/uberspace/mail-autoresponder

Setup mail accounts with auto responder on an uberspace. 

Enable the autoresponder by creating an IMAP folder called "Autorespond" 
in the root of your IMAP folders (not in INBOX, but as a sibling of INBOX).

Once the folder is present and it contains a message, the autoresponder is 
turned on and it will respond with the most recent message in this 
folder. 

## Requirements

Setup an [U7 Uberspace](https://dashboard.uberspace.de/register) and provide an ssh access to it.

## Role Variables

See [defaults/main.yml](defaults/main.yml). 

`mail_autoresponder_users` is a list of dictionaries.
Each item must have a `name`. You don't need to set the `domain`. 
If you do, this defines the sender of the autoresponse.

```
FROM="{{ mail_autoresponder_user }}@{{ mail_autoresponder_domain }}"
```

# Otherwise, your uberspace address will be used

```
FROM="{{ mail_autoresponder_user }}@{{ ansible_facts.user_id }}.uber.space"
```

`enable_log` defaults to false. It activates a mail log for debugging purposes.

```yaml
mail_autoresponder_users:
  - name: info
#  - name: jane
#    domain: family-doe.com
#  - name: bob
#    enable_log: true
#  - name: alice
#    domain: wonder.land
#    enable_log: true
```

## Example Playbook

### Prerequisites

In your project, provide the following files:

ansible.cfg

```ini
[defaults]
roles_path = $PWD/galaxy_roles:$PWD/roles
```

requirements.yml
 
```yaml
- src: git+https://gitlab.com/macrominds/provision/uberspace/mail-autoresponder.git
  path: roles
  name: mail-autoresponder
```

And run `ansible-galaxy install -r requirements.yml` to install 
or `ansible-galaxy install -r requirements.yml  --force` to upgrade.

### Playbook

```yaml
- hosts: all
  roles:
    - role: mail-autoresponder
      mail_autoresponder_users:
        - name: john
        - name: jane
          domain: family-doe.com
        - name: bob
          enable_log: true
        - name: alice
          domain: wonder.land
          enable_log: true
```

## Testing

Test this role with `molecule test --all`.

## License

ISC

## Author Information

This role was created in 2020 by Thomas Praxl.
